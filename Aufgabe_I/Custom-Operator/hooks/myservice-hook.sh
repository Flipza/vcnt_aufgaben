#!/usr/bin/env bash

if [[ $1 == "--config" ]]
then
  cat <<EOF
{
  "configVersion":"v1",
  "kubernetes":[{
    "apiVersion": "education.tbz.ch/v1",
    "kind": "MyService",
    "executeHookOnEvent":["Added", "Deleted"]
  }]
}
EOF

else

    # ignore Synchronization for simplicity
    type=$(jq -r '.[0].type' $BINDING_CONTEXT_PATH)
    if [[ $type == "Synchronization" ]]
    then
        echo Got Synchronization event
        exit 0
    fi

    resourceEvent=$(jq -r '.[0].watchEvent' $BINDING_CONTEXT_PATH)
    name=$(jq -r '.[0].object.metadata.name' ${BINDING_CONTEXT_PATH})

    if [[ $resourceEvent == "Added" ]]
    then

      image=$(jq -r '.[0].object.spec.image' ${BINDING_CONTEXT_PATH})
      replicas=$(jq -r '.[0].object.spec.replicas' ${BINDING_CONTEXT_PATH})
      port=$(jq -r '.[0].object.spec.port' ${BINDING_CONTEXT_PATH})

      kubectl create deployment ${name} --image=${image}
      kubectl scale --replicas=${replicas} deployment/${name}
      kubectl expose deployment/${name} --type="LoadBalancer" --port ${port}
      echo "${kind}/${name} object is added"

    # delete
    else

      kubectl delete deployment/${name}
      kubectl delete service/${name}
      echo "${kind}/${name} object is deleted"

    fi

fi