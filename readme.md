# Aufgabe A - Jobs verteilt ausführen

- Anleitung von Kubernetes.io befolgen: https://kubernetes.io/docs/tasks/job/coarse-parallel-processing-work-queue/

  Dies beinhaltet die Installation und Erstellung einer Job-Queue mit dem Namen "job1". 
  Diese Job-Queue läuft auf einem separaten Container und ist jederzeit über den Abruf "amqp://guest:guest@rabbitmq-service:5672" verfügbar. Die Queue wird danach aus einem weiteren temporären Container mit 8 Texten/Wörtern befüllt, sodass 8 Inputs in der Queue zur Verarbeitung bereitliegen.

- Dieses Beispiel verwendet Python und ein Python-Script, welches beim Ausführen des Jobs bzw. der Pods auf allen Pods ausgeführt wird.

- Python-Script "skript.py":

```
#!/usr/bin/env python

import subprocess

bashCommand = "wget shorturl.at/bE156"
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output, error = process.communicate()
```

  Dieses Skript ruft über wget die folgende URL auf: shorturl.at/bE156
  Dabei wird auf dieser URL einen Counter um 1 erhöht, sodass überprüft werden kann, ob die URL auch wirklich 8 mal von den verteilt ausgeführten Jobs aufgerufen wurde.

- Die Container werden mit dem Dockerfile mit python und wget ausgestattet und dabei wird angegeben, dass die Python Befehle im skript.py ausgeführt werden sollen.

- dockerfile:

```
# Specify BROKER_URL and QUEUE when running
FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y curl ca-certificates amqp-tools python wget \
       --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*
COPY ./skript.py /skript.py

CMD  /usr/bin/amqp-consume --url=$BROKER_URL -q $QUEUE -c 1 /skript.py
```

- Die Container Repo wird erstellt ("docker build" -Befehl) und auf gitlab.com gepusht mit folgenden Befehlen:

  **sudo docker build -t registry.gitlab.com/cntmodul/job-gruppe1-1** .
  
  **sudo docker push registry.gitlab.com/cntmodul/job-gruppe1-1**


- Die definition des Jobs mit namen "**job-gruppe1-1**", welcher die Pods - maximal 8 davon, 2 aufs Mal - anhand der Job-Queue erstellt, wird mittels yaml Datei gemacht. Die Container für die Pods werden direkt aus dem gitlab.com Repo geladen.

- job-gruppe1.yaml:

```
apiVersion: batch/v1
kind: Job
metadata:
  name: job-gruppe1-1
spec:
  completions: 8
  parallelism: 2
  template:
    metadata:
      name: job-gruppe1-1
    spec:
      containers:
      - name: c
        image: registry.gitlab.com/cntmodul/job-gruppe1-1
        env:
        - name: BROKER_URL
          value: amqp://guest:guest@rabbitmq-service:5672
        - name: QUEUE
          value: job1
      restartPolicy: OnFailure
```

- Danach wird der Job mit folgendem Befehl gestartet:

  **kubectl apply -f ./job-gruppe1.yaml**

- Die Ausführung kann übers Kobernetes Dashboard oder auch mit folgendem Befehl kontrolliert werden:

   **kubectl describe jobs/job-gruppe1-1**

- Nach erfolgreicher Ausführung ist der Counter auf der URL shorturl.at/bE156 um 8 gestiegen.



# Aufgabe B Health Probe Pattern


## Livenessprobe

Hier wird eine einfache Livenessprobe beschrieben.

In dem Beispiel wird ein Deployment erstellt, welches das Image busybox verwendet. 
Innerhalb der busybox wird dann die Shell geöffnet und und eine Datei namens "**Hello**" erstellt. 
Diese Datei wird nach 20 Sekunden gelöscht.

Die Livenessprobe wird mit dem Exec Command durchgeführt. Das Command ist cat Hello. Wenn der Exic Code dieses Kommandos 0 ist, wird die Livenessprobe als bestanden angesehen.

- livenessprobe.yaml:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: livenessprobe
  name: livenessprobe
spec:
  replicas: 1
  selector:
    matchLabels:
      app: livenessprobe
  template:
    metadata:
      labels:
        app: livenessprobe
    spec:
      containers:
      - image: busybox
        name: busybox
        args:
          - /bin/sh
          - -c
          - touch hello; sleep 20; rm -f hello; sleep 600
        livenessProbe:
          exec:
            command:
              - cat 
              - hello 
          initialDelaySeconds: 5
          periodSeconds: 3
```

Wir definieren am Schluss noch zwei Optionen:
- "**initialDelaySeconds**" welche auf 5 Sekunden sind. Dies bedeutet, dass wenn der Container startet, wird der erste Livenesscheck erst nach 5 Sekunde starten, damit noch keine Fehlermeldung entsteht, da der Container noch nicht oben ist.
- "**periodSeconds**" bedeutet, dass der Livenesscheck alle 3 Sekunden durchgeführt wird.

Der Ablauf ist nun wiefolgt: In den ersten 20 Sekunden nach erfolgreichem Start wird die Livenessprobe als erfolgreich angesehen und alles läuft einwandfrei.
Nach den 20 Sekunden wird die Livenessprobe fehlschlagen und der Kubernetescluster wird den Pod neu starten.

Wir starten das Deployment mit folgendem befehl:
- **kubectl create -f livenessprobe.yaml**

Anschliessend können wir prüfen, ob das Deployment gestartet ist:
- **kubectl get deploy**

Und in den Events überprüfen, was abgeht:
- **kubectl get Events**



## Readinessprobe

Mit der Readinessprobe verfolgen wir fast den gleichen Ansatz wie oben bei der Livenessprobe.

Im Unterschied zu Livenessprobe warten wir hier 20 Sekunden mit der Erstellung der "Hello" Datei. Wenn das Deployment startet wird somit die Readiness noch fehlschlagen. Erst nach diesen 20 Sekunden wird die "Hello" Datei erstellt und somit wird die Readiness auf Ready gestellt.

- readinessprobe.yaml:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: readinessprobe
  name: readinessprobe
spec:
  replicas: 1
  selector:
    matchLabels:
      app: readinessprobe
  template:
    metadata:
      labels:
        app: readinessprobe
    spec:
      containers:
      - image: busybox
        name: busybox
        args:
          - /bin/sh
          - -c
          - sleep 20; touch hello; sleep 600
        readinessProbe:
          exec:
            command:
              - cat 
              - hello 
          initialDelaySeconds: 15
          periodSeconds: 3
```

Wir starten das Deployment mit folgendem befehl:
- kubectl create -f readinessprobe.yaml

Wir prüfen, ob der Pod Ready ist:
kubectl get all

Anschliessend warten wir für ein paar Sekunden. Nach ca 20 Sekunden sollte der Pod Ready sein:
watch kubectl get all

# Aufgabe C - init Container

In diesem Beispiel gehen wir darauf ein, wie ein init Container verwendet werden kann. Die Idee in diesem Beispiel besteht darin, wass wir im init Container ein Shared Volume erstellen, in welchem eine HTML Datei generiert wird. Sobald dieses erstsellt wird, startet der eigentliche Webserver (Nginx), welches das vorbereitete Shared Volume im Stammverzeichnis von Nginx mounted:

- init_container_zahner.yaml:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    run: nginx
  name: nginx-deploy
spec:
  replicas: 1
  selector:
    matchLabels:
      run: nginx
  template:
    metadata:
      labels:
        run: nginx

    spec:

      volumes:
      - name: shared-volume
        emptyDir: {}

      initContainers:
      - name: busybox
        image: busybox
        volumeMounts:
        - name: shared-volume
          mountPath: /nginx-data
        command: ["/bin/sh"]
        args: ["-c", "echo '<h1>Hello Kubernetes</h1>' > /nginx-data/index.html"]

      containers:
      - image: nginx
        name: nginx
        volumeMounts:
        - name: shared-volume
          mountPath: /usr/share/nginx/html
```

Wir starten das Deployment mit folgendem Befehl:
-  kubectl create -f init_container_zahner.yaml

Wir können den Status des Deployments mit folgendem Befehl überprüfen:
- kubectl get all

Sobald das Deployment namens "nginx-deploy" gestartet ist, können wir eine Portweiterleitung konfigurieren. Dies machen wir heir mit NodePort:
- kubectl expose deployment nginx-deploy --type NodePort --port 80

Wir können die Funktion überprüfen, indem wir nochmal folgenden Befehl eingeben:
- kubectl get service

Anschliessend können wir beim Service nginx-deploy den gemappten Port auslesen:
- nginx-deploy       NodePort       10.152.183.3     <none>        80:31883/TCP     35m

In unserem Fall wird der Port 80 auf Port 31883 gemappt. Nun müssen wir nur noch die WorkerNode IP Adresse in unserem Kluster nachschauen und den Port dahinter hängen. In unserem Fall können wir jetzt den Browser öffnen und folgende IP eingeben:
- http://10.1.39.5:31883/


# Aufgabe D - Horizontal Pod Autoscaler

In diesem Beispiel wird ein Webserver Service simuliert, welcher automatisch zwischen 1 bis 10 Pods skalieren kann.
Ausschlaggebend bei der Skalierung ist die CPU Nutzung. In unserem Beispiel wird der Wert 500m pro Pod als Maximum Auslastung definiert. Pro CPU ist maximal 1000m möglich.

- php-apache.yaml:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: php-apache
spec:
  selector:
    matchLabels:
      run: php-apache
  replicas: 1
  template:
    metadata:
      labels:
        run: php-apache
    spec:
      containers:
      - name: php-apache
        image: registry.k8s.io/hpa-example
        ports:
        - containerPort: 80
        resources:
          limits:
            cpu: 500m
          requests:
            cpu: 200m

---

apiVersion: v1
kind: Service
metadata:
  name: php-apache
  labels:
    run: php-apache
spec:
  ports:
  - port: 80
  selector:
    run: php-apache

---

apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: php-apache
  namespace: default
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: php-apache
  minReplicas: 1
  maxReplicas: 10
  targetCPUUtilizationPercentage: 50
```

Wir starten das deployment mit folgendem Befehl:
- kubectl create -f php-apache.yaml

Wir können nun die Auslastung mit nachfolgendem Befehl überprüfen:
- kubectl get hpa

Der Output sollte wie nachfolgend aussehen:
```
NAME         REFERENCE                     TARGET    MINPODS   MAXPODS   REPLICAS   AGE
php-apache   Deployment/php-apache/scale   0% / 50%  1         10        1          18s
```

Nun können wir mit einem mit folgendem Befehl die Auslastung des Containers erhöhen:
- kubectl run -i --tty load-generator --rm --image=busybox:1.28 --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache; done"

Um die Auslastung des Deployments zu überwachen, können wir nun die Auslastung antzeigen. Hierbei ist der Wert unter "Target" ausschlaggebend:
- kubectl get hpa php-apache --watch

Nach ein paar Minuten sollte eine höhere CPU Auslastung ersichtlich sein:
```
NAME         REFERENCE                     TARGET      MINPODS   MAXPODS   REPLICAS   AGE
php-apache   Deployment/php-apache/scale   256% / 50%  1         10        1          3m
```

Nocheinmal ein paar Minuten später sollten nun mehr Replicas ersichtlich sein:
```
NAME         REFERENCE                     TARGET      MINPODS   MAXPODS   REPLICAS   AGE
php-apache   Deployment/php-apache/scale   305% / 50%  1         10        7          3m
```

Wir können dies mit nachfolgenden Befehl überprüfen:
- kubectl get deployment php-apache

# Aufgabe E - RBAC

Als erstes haben wir ein User Zertifikat angelegt. Um dies zu machen, muss man sich jedoch mit sudo -i anmelden
- sudo -i

Mithilfe des folgenden Befehls wird dann effektiv das der private Key und das CSR erstellt: 
- openssl genrsa -out hansli.pem 2048
- openssl req -new -key hansli.pem -out hansli.csr -subj "/CN=hansli"

Es entstehen die Dateien "hansli.pem" und "hansli.csr"

Sobald diese Schritte durchgeführt wurden, muss man wieder aus dem sudo -i aussteigen, um den CSR dann in Kubernetes als Ressource zu erstellen. Dazu wird der Befehl unten verwendet:

```
- cat <<%EOF% | kubectl apply -f -
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: user-request-hansli
spec:
  signerName: kubernetes.io/kube-apiserver-client
  groups:
  - system:authenticated
  request: $(cat hansli.csr | base64 | tr -d '\n')
  usages:
  - digital signature
  - key encipherment
  - client auth
%EOF%
```

Damit das CSR bzw Zertifikat approved wird, muss noch folgender Befehl ausgeführt werden: 
- "kubectl certificate approve user-request-hansli"

Zum Schluss noch den CSR signieren:
- kubectl get csr user-request-hansli -o jsonpath='{.status.certificate}' | base64 -d > hansli.crt


Um überhaupt den User Account verwenden zu können, wird eine K8s' Konfig gebraucht. Diese wird in diesem Verzeichnis abgelegt: .kube/config-hansli
Die Befehle für die Erstellung sind wie folgt: 
- kubectl --kubeconfig ~/.kube/config-hansli config set-cluster kubernetes --insecure-skip-tls-verify=true --server=https://10.0.44.17:16443
- kubectl --kubeconfig ~/.kube/config-hansli config set-credentials hansli --client-certificate=hansli.crt --client-key=hansli.pem --embed-certs=true

Als nächster Schritt muss noch die Role und das Rolebinding erzeugt werden. Für das wird eine YAML Datei erstellt: 
```
- kind: Role
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  namespace: default
  name: pod-reader-role
rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["get", "watch", "list"]
---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: pod-reader-rolebinding
  namespace: default
subjects:
- kind: User
  name: hansli
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: pod-reader-role
  apiGroup: rbac.authorization.k8s.io
```

Danach muss ddas YAML File noch ausgeführt werden:
- kubectl apply -f rbac-hansli.yaml

Um dies zu testen, können noch die Pods geholt werden:
- kubectl --kubeconfig .kube/config-hansli get pods   


# Aufgabe H - Helm - Kubernetes Paketmanager
Als erstes muss Helm installiert werden, dies lässt sich mit folgendem Befehl:
```
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
```

Mittels des Befehles kann man nach spezifischen Repos suchen: 
```
Helm search repo «applikation»
```
In unserem Fall haben wir nach nginx gesucht: 
```
Helm search repo nginx
```
Wenn man die repo adden will:
```
Helm repo add bitnami https://charts.bitnami.com/bitnami
```
Als nächstes der Download von nginx
```
Helm pull bitnami/nginx –untar-true
```
Damit warden alle templates und yaml Files heruntergeladen, anschliessend lässt sich nginx installieren:
```
Helm install helm-nginx bitnami/nginx
```
Das helm nginx Chart ist nun installiert als neues release. Jede installation erstellt dabei ein neues release, somimt können Helm Charts beliebig viel mal installiert werden (z.B: für mehrere MySQL Server etc)
Die installierten releases können dabei folgendermassen angeschaut/geprüft werden:
```
Helm status helm-nginx
```
Desweiteren kann mit...:
```
kubectl get all
```
...geprüft werden, ob die nötigen pods und services laufen.
Und züsätzlich können mit dem folgenden Helm-Befehl alle installierten releases angezeigt werden:
```
Helm list
```
Mit dem export Befehl können die ports und ip's der installierten services nach aussen gemappt werden:
```
export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services helm-nginx)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
```
Dann wird überprüft, ob das neu installierte nginx release auf unserem Cluster erreichbar ist:
```
kubectl get service
```
Dort wird die external ip und port angezeigt
Die Verbindung kann im Browser durch Aufrufen der ip und port überprüft werden.

Um ein installiertes release zu deinstallieren kann mit...:
```
Helm list
```
...das release aufgelistet werden und dann mit dem folgenden Befehl komplett vom cluster entfernt / deinstalliert werden, dies kann so aussehen (release nummer am Ende des Namens):
```
Helm uninstall nginx-1664474278
```
Nach der Bestätigung "...uninstalled" kann man erneut überprüfen, ob die pods und services erfolgreich deinstalliert wurden:
```
kubectl get all
```

# Aufgabe I - Operator Pattern

Wir erstellen eine eigene Ressource (Custom Resource Definition), mit der Kubernetes erweitert wird.

Dabei wird über die Funktionalität von Operator Patterns die Aufgabe erleichtert, mehrere Pods gleichzeitig über ein Replica Set zu deployen und diese über einen Service bereitzustellen.

Dadurch kann durch sehr wenig Aufwand eine ganze Umgebung aufgesetzt und wieder sauber von Kubernetes entfernt werden.

In diesem Beispiel wird ein einfacher Webserver mit einer Landing-Page über 3 Pods deployed, welche durch den Load Balancer angesteuert werden.

Das verwendete Image ist ein hello-world Beispielimage, welches alles nötige für die Darstellung der Webseite beinhaltet.

RBAC muss dabei deaktiviert sein, denn ansonsten werden die kubectl Befehle auf der hooks.sh Datei nicht ausgeführt:


```
microk8s disable rbac
```


Folgende Dateien werden für dieses Beispiel benötigt:

- hooks/hooks.sh

--> Definiert die Trigger, welche beim Erstellen oder Entfernen der Pods ausgeführt werden.

- Dockerfile	

--> wird fürs Erstellen des shell operators benötigt

- myservice-operator-rbac.yaml	

--> benötigt für ServiceAccount, ClusterRole, ClusterRoleBinding

- myservice-operator.yaml	

--> definiert den Pod für den operator (muss zusätzlich zu den vom ReplicaSet deployten Pods laufen)

- myservice.yaml	

--> definiert die Pods vom Typ der Custom Ressource und die Anzahl an Replicas.

- resourcedefinition.yaml	

--> definiert die Custom Resource und den Standard Port für den Zugriff, wenn dieser im myservice.yaml nicht angegeben wird.


Folgende Befehle werden dafür benötigt:

```
kubectl apply -f resourcedefinition.yaml
kubectl apply -f myservice-operator-rbac.yaml
kubectl apply -f myservice-operator.yaml
kubectl apply -f myservice.yaml
```

Damit werden die Pods, ReplicaSet, Service, Deployment Ressourcen inklusive der Custom Resource Definition erstellt.


# Aufgabe J - Monitoring



Um die Monitoring Tools zu installieren, wird das Open Source Monitoring Stack gebraucht. Um die Installation zu vereinfachen wird dafür helm verwendet.



Zusätzlich wird noch ein Namespace gebraucht mit dem Namen "Monitoring". Danach kann man das Helm Chart von Open Source Monitoring Stack verwenden.



```
kubectl create namespace monitoring
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install my-kube-prometheus-stack prometheus-community/kube-prometheus-stack --version 41.9.0 -n monitoring
```



Im Stack sind verschiedene Monitoring Tools enthalten, mit denen man dann arbeiten kann:
- Grafana
- Prometheus
- Alertmanager



Damit das UI aufgerufen werden kann, müssen die Ports der User Interfaces geändert werden. (ClusterIP zu Loadbalancer)



Die Ports findet man mit folgendem Befehl:



```
kubectl -n monitoring get services
```



Danach die richtige IP annehmen und sich mit den Credentials anmelden.



In der Prometheus UI kann mittels PromQL die gewünschte Ressource ausgewählt werden. Wir haben dafür folgendens Query genommen:



```
apiserver_storage_objects
```
